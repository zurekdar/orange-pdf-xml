﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports iTextSharp.text.pdf

Module mod_Main

    ' Path
    Private exe_Path As String = Reflection.Assembly.GetExecutingAssembly.Location
    Private app_Path As String = exe_Path.Substring(0, exe_Path.LastIndexOf("\"))
    Private ini_Path As String = app_Path & "\app_XfaToPdf.ini"
    Private ini_Read As New cls_Utils("12345678")

    ' Sql
    Private sql_DataSource As String = ini_Read.ReadIni(ini_Path, "Connection SQL", "ServerSQL")
    Private sql_InitialCatalog As String = ini_Read.ReadIni(ini_Path, "Connection SQL", "InitialCatalog")
    Private sql_UserID As String = ini_Read.DecryptUrl(ini_Read.ReadIni(ini_Path, "Connection SQL", "UserSQL"))
    Private sql_UserPassword As String = ini_Read.DecryptUrl(ini_Read.ReadIni(ini_Path, "Connection SQL", "UserPass"))

    ' Eml
    Private eml_DataSource As String = ini_Read.ReadIni(ini_Path, "Connection EML", "ServerSQL")
    Private eml_InitialCatalog As String = ini_Read.ReadIni(ini_Path, "Connection EML", "InitialCatalog")
    Private eml_UserID As String = ini_Read.DecryptUrl(ini_Read.ReadIni(ini_Path, "Connection EML", "UserSQL"))
    Private eml_UserPassword As String = ini_Read.DecryptUrl(ini_Read.ReadIni(ini_Path, "Connection EML", "UserPass"))

    ' App
    Private app_Pattern As String = ini_Read.ReadIni(ini_Path, "App", "Pattern")
    Private app_Documents As String = ini_Read.ReadIni(ini_Path, "App", "Documents")

    ' Connection SQL
    Private _connectionSql As New SqlConnectionStringBuilder(String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3}" _
                                                        , sql_DataSource _
                                                        , sql_InitialCatalog _
                                                        , sql_UserID _
                                                        , sql_UserPassword)) With {.MultipleActiveResultSets = True, .AsynchronousProcessing = True}

    ' Connection Eml
    Private _connectionEml As New SqlConnectionStringBuilder(String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3}" _
                                                        , eml_DataSource _
                                                        , eml_InitialCatalog _
                                                        , eml_UserID _
                                                        , eml_UserPassword)) With {.MultipleActiveResultSets = True, .AsynchronousProcessing = True}

    Sub Main()
        Dim _source As String = app_Path + app_Pattern
        Dim _dt As New DataTable
        Dim _da As New SqlDataAdapter

        Using connection As New SqlConnection(_connectionSql.ConnectionString)
            connection.Open()
            '
            Using command As New SqlCommand
                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandTimeout = 0
                '
                command.CommandText = "_select_xfa"
                command.Parameters.Clear()

                _da.SelectCommand = command
                _da.Fill(_dt)
            End Using
        End Using

        PdfReader.unethicalreading = True

        For Each _row As DataRow In _dt.Rows
            Dim _id As String = _row("_id").ToString
            Dim _xml As String = _row("_xml").ToString

            Dim _stream As New MemoryStream(Encoding.UTF8.GetBytes(_xml))

            Using _reader As New PdfReader(_source)
                Using _ms As New MemoryStream()
                    Using _stamper As New PdfStamper(_reader, _ms, ControlChars.NullChar, True)
                        Dim _xfa As New XfaForm(_reader)
                        Dim _fields As AcroFields = _stamper.AcroFields

                        _fields.Xfa.FillXfaForm(_stream)
                        _xfa.Changed = True

                        XfaForm.SetXfa(_xfa, _stamper.Reader, _stamper.Writer)
                    End Using

                    Dim bytes As Byte() = _ms.ToArray()
                    System.IO.File.WriteAllBytes(app_Path + app_Documents + "\nowy_" + _id + ".pdf", bytes)
                End Using
            End Using
        Next
    End Sub
End Module
