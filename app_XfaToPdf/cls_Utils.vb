﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Security.Cryptography
Imports System.Text.RegularExpressions
Imports System.Collections.Specialized
Imports System.Text
Imports System.Runtime.InteropServices

Public Class cls_Utils
    ' -----------------------------------------------------------------------------
    ' INI
    ' -----------------------------------------------------------------------------
    <DllImport("kernel32.dll", SetLastError:=True)>
    Private Shared Function WritePrivateProfileString(
        ByVal lpAppName As String,
        ByVal lpKeyName As String,
        ByVal lpString As String,
        ByVal lpFileName As String) As Integer
    End Function

    <DllImport("kernel32.dll", CharSet:=CharSet.Unicode, SetLastError:=True)>
    Private Shared Function GetPrivateProfileString(
        ByVal lpAppName As String,
        ByVal lpKeyName As String,
        ByVal lpDefault As String,
        ByVal lpReturnedString As String,
        ByVal nSize As Integer,
        ByVal lpFileName As String) As Integer
    End Function

    <DllImport("kernel32.dll", CharSet:=CharSet.Unicode, SetLastError:=True)>
    Private Shared Function GetPrivateProfileInt(
        ByVal lpAppName As String,
        ByVal lpKeyName As String,
        ByVal nDefault As Integer,
        ByVal lpFileName As String) As Integer
    End Function

    Public Function ReadIni(ByVal strIniFile As String, ByVal strKey As String, ByVal strItem As String) As String
        Dim _string As String = New String(String.Empty, 512)
        Dim _size As Integer = GetPrivateProfileString(strKey, strItem, "", _string, _string.Count, strIniFile)

        If _size = 0 Then
            Return ""
        End If

        Dim _null As String = _string.ToString.Trim(New String(vbNullChar, 1))

        Return _null
    End Function

    Public Shared Function WriteIni(ByVal strIniFile As String, ByVal strKey As String, ByVal strItem As String, ByVal strValue As String) As Boolean
        Return WritePrivateProfileString(strKey, strItem, strValue, strIniFile)
    End Function

    ' Dim s As String = cls_Utils.ReadIni("d:\sample.ini", "MAIL", "Mapixver")
    ' WritePrivateProfileString("default", "sample_key", "hello_world", "d:\sample.ini")

    Private _code As String

    Public Sub New(ByVal _code As String)
        Me._code = _code
    End Sub

    ' -----------------------------------------------------------------------------
    ' CRYPT
    ' -----------------------------------------------------------------------------

    ' DEKODOWANIE
    Public Function DecryptUrl(_string As String) As String
        Dim _byte As Byte() = Convert.FromBase64String(_string.Replace("*", "=").Replace("_", "/").Replace("-", "+"))
        Dim _key As Byte() = System.Text.Encoding.UTF8.GetBytes(_code)
        Dim _iv As Byte() = HexStringToByteArray(_code + _code)

        Using _des As New DESCryptoServiceProvider
            Using _ms As New MemoryStream
                Using _cs As New CryptoStream(_ms, _des.CreateDecryptor(_key, _iv), CryptoStreamMode.Write)
                    _cs.Write(_byte, 0, _byte.Length)
                    _cs.FlushFinalBlock()
                End Using
                Return System.Text.Encoding.UTF8.GetString(_ms.ToArray())
            End Using
        End Using
    End Function

    ' KODOWANIE
    Public Function EncryptUrl(_string As String) As String
        Dim _byte As Byte() = Encoding.UTF8.GetBytes(_string)
        Dim _key As Byte() = System.Text.Encoding.UTF8.GetBytes(_code)
        Dim _iv As Byte() = HexStringToByteArray(_code + _code)

        Using _des As New DESCryptoServiceProvider
            Using _ms As New MemoryStream
                Using _cs As New CryptoStream(_ms, _des.CreateEncryptor(_key, _iv), CryptoStreamMode.Write)
                    _cs.Write(_byte, 0, _byte.Length)
                    _cs.FlushFinalBlock()
                End Using
                Return Convert.ToBase64String(_ms.ToArray()).Replace("=", "*").Replace("/", "_").Replace("+", "-")
            End Using
        End Using
    End Function

    ' TRIPLE KODOWANIE
    Public Function EncryptTriple(_string As String) As String
        Dim _byte As Byte() = Encoding.UTF8.GetBytes(_string)
        Dim _key As Byte() = HexStringToByteArray("010203040506070801020304050607080102030405060708")
        Dim _iv As Byte() = HexStringToByteArray("0102030405060708")

        Using _des As New TripleDESCryptoServiceProvider() With {.Key = _key, .IV = _iv, .Mode = CipherMode.CBC, .Padding = PaddingMode.PKCS7}
            Using _ms As New MemoryStream
                Using _cs As New CryptoStream(_ms, _des.CreateEncryptor(), CryptoStreamMode.Write)
                    _cs.Write(_byte, 0, _byte.Length)
                    _cs.FlushFinalBlock()
                End Using
                Return GetHexString(_ms.ToArray())
            End Using
        End Using
    End Function

    ' TRIPLE DEKODOWANIE
    Public Function DecryptTriple(_string As String) As String
        Dim _byte As Byte() = HexStringToByteArray(_string)
        Dim _key As Byte() = HexStringToByteArray("010203040506070801020304050607080102030405060708")
        Dim _iv As Byte() = HexStringToByteArray("0102030405060708")

        Using _des As New TripleDESCryptoServiceProvider() With {.Key = _key, .IV = _iv, .Mode = CipherMode.CBC, .Padding = PaddingMode.PKCS7}
            Using _ms As New MemoryStream
                Using _cs As New CryptoStream(_ms, _des.CreateDecryptor(), CryptoStreamMode.Write)
                    _cs.Write(_byte, 0, _byte.Length)
                    _cs.FlushFinalBlock()
                End Using
                Return System.Text.Encoding.UTF8.GetString(_ms.ToArray())
            End Using
        End Using
    End Function

    Private Function HexStringToByteArray(ByVal _hex As String) As Byte()
        Dim B As Byte() = Enumerable.Range(0, _hex.Length).Where(Function(x) x Mod 2 = 0).[Select](Function(x) Convert.ToByte(_hex.Substring(x, 2), 16)).ToArray()
        Return Enumerable.Range(0, _hex.Length).Where(Function(x) x Mod 2 = 0).[Select](Function(x) Convert.ToByte(_hex.Substring(x, 2), 16)).ToArray()
    End Function

    Private Function GetHexString(_byte As Byte())
        With New StringBuilder(_byte.Length * 3)
            For Each _b As Byte In _byte
                .AppendFormat("{0:x2}", _b)
            Next
            Return (.ToString.Trim)
        End With
    End Function

    ' QUERY STRING
    Public Function QueryStringsEncryptUrl(_string As NameValueCollection) As String
        Dim strings As New StringBuilder()
        For Each stringKey As String In _string.Keys
            If strings.Length > 0 Then
                strings.Append("&")
            End If
            strings.Append(String.Format("{0}={1}", stringKey, _string(stringKey)))
        Next
        Return EncryptUrl(strings.ToString())
    End Function

    ' SQL
    Public Function SelectScalarString(command As SqlCommand) As String
        Dim _scalar As Object = Nothing
        Dim _string As String = ""
        _scalar = command.ExecuteScalar
        '
        If IsNothing(_scalar) = False Then
            If DBNull.Value.Equals(_scalar) = False Then
                _string = _scalar
            End If
        End If
        Return _string
    End Function

    Public Function SelectScalarGuid(command As SqlCommand) As Guid
        Dim _scalar As Object = Nothing
        Dim _guid As New Guid("00000000-0000-0000-0000-000000000000")
        _scalar = command.ExecuteScalar
        '
        If IsNothing(_scalar) = False Then
            If DBNull.Value.Equals(_scalar) = False Then
                _guid = _scalar
            End If
        End If
        Return _guid
    End Function

    Public Function SelectScalarInt64(command As SqlCommand) As Int64
        Dim _scalar As Object = Nothing
        Dim _id As Int64 = 0
        _scalar = command.ExecuteScalar
        '
        If IsNothing(_scalar) = False Then
            If DBNull.Value.Equals(_scalar) = False Then
                _id = Convert.ToInt64(_scalar)
            End If
        End If
        Return _id
    End Function

    Public Function GridDbnull(s As Object) As Boolean
        If DBNull.Value.Equals(s) Then
            Return True
        End If
        Return False
    End Function

    ' PARSE
    Public Function BooleanParse(s As Object) As Boolean
        If Not DBNull.Value.Equals(s) Then
            Return s
        End If
        Return False
    End Function

    Public Function Int64Parse(s As Object) As Int64
        Dim num As Int64 = 0
        If Not DBNull.Value.Equals(s) Then
            If Not Int64.TryParse(s, num) Then
                Return 0
            End If
            Return num
        Else
            Return 0
        End If
        Return num
    End Function

    Public Function StringParse(s As Object) As String
        If Not DBNull.Value.Equals(s) Then
            Return s
        End If
        Return String.Empty
    End Function

    ' FILE
    Public Function GetFilesRecursive(ByVal initial As String, ByVal mask As String) As List(Of String)
        Dim result As New List(Of String)
        Dim stack As New Stack(Of String)
        stack.Push(initial)

        Do While (stack.Count > 0)
            Dim dir As String = stack.Pop
            Try
                result.AddRange(Directory.GetFiles(dir, mask))
                Dim directoryName As String
                For Each directoryName In Directory.GetDirectories(dir)
                    stack.Push(directoryName)
                Next

            Catch ex As Exception
            End Try
        Loop
        Return result
    End Function

End Class
