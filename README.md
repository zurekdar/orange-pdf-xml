## itextsharp

xfa form + xml to pdf

- itextsharp.dll: the core library
- itextsharp.xtra.dll: extra functionality (PDF 2!)
- itextsharp.pdfa.dll: PDF/A-related functionality
You can find the latest release here:
http://sourceforge.net/projects/itextsharp/files/itextsharp/

iTextSharp is licensed as AGPL software.

---

	Dim _xml = "
	<xfa:datasets xmlns:xfa=""http://www.xfa.org/schema/xfa-data/1.0/"">
	  <xfa:data>
		<Deklaracja xmlns=""http://crd.gav.org/wzor/2019/11/08/8836/"">
		  <PozycjeSzczegolowe>
			<P_10>102222</P_10>
			<P_11>112222</P_11>
			<P_12>12222</P_12>
			<P_13>13222</P_13>
			<P_14>1422</P_14>
			<P_15>15222</P_15>
			<P_16>1622</P_16>
			<P_17>17222</P_17>
			<P_18>1822</P_18>
			<P_19>19222</P_19>
		  </PozycjeSzczegolowe>
		</Deklaracja>
	  </xfa:data>
	</xfa:datasets>
	"
	
	Dim _stream As New MemoryStream(Encoding.UTF8.GetBytes(_xml))

	Using _reader As New PdfReader(_source)
		Using _ms As New MemoryStream()
			Using _stamper As New PdfStamper(_reader, _ms, ControlChars.NullChar, True)
				Dim _xfa As New XfaForm(_reader)
				Dim _fields As AcroFields = _stamper.AcroFields

				_fields.Xfa.FillXfaForm(_stream)
				_xfa.Changed = True

				XfaForm.SetXfa(_xfa, _stamper.Reader, _stamper.Writer)
			End Using

			Dim bytes As Byte() = _ms.ToArray()
			System.IO.File.WriteAllBytes(app_Path + app_Documents + "\nowy_" + _id + ".pdf", bytes)
		End Using
	End Using	
	
	
---